# HUI

#### 介绍
本UI框架命名为“HUI”，全名”HarmonyOS ArkUI eTS UI框架“，“H”是指HarmonyOS，是基于鸿蒙方舟开发框架、使用eTS语言量身打造的鸿蒙App开发框架，可以作为开发新App的脚手架。本框架严格遵循”ArkUI App设计规范“。随着《鸿蒙开发ArkUI最佳实践》系列课程的推进节奏，会持续丰富本框架。

#### 软件架构
HarmonyOS 3.0 Beta2
